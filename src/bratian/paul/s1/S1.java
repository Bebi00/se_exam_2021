package bratian.paul.s1;

import java.util.ArrayList;

public class S1 {


 class A{

 }

 class U{
     private void methodB(B b){
         // from the dependency relationship ( class B might be used)
         // a parameter of class B is necessary
     }
 }

 class E{

 }
 class C{

 }
 class B extends A{ // from the inheritance relationship
     private String param;
     private D attributeD; // from the association relationship

     ArrayList<E> eArrayList = new ArrayList<>();
     // the aggregation relationship implies that the object of the E class
     // can be added later because their life cycle is independent in comparison to the life cycle of the B class

     ArrayList<C> cArrayList = new ArrayList<>();
     B(){
         cArrayList.add(new C()); // from the composition relationship
         // the object of class C has to be constructed when the class B is constructed
         // because it has a life cycle dependent to this class
     }

     public void x(){

     }
     public void y(){

     }



 }

 class D{

 }
}

