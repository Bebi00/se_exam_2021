package bratian.paul.s2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class S2 extends JFrame {
    public static void main(String[] args) {
        new S2();
    }

    // declaration of the GUI elements
    TextField textField;
    TextArea textArea;
    Button button;

    S2() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        init();
    }
        void init() { // function used for initializing the GUI window and elements
            setLayout(null);
            setSize(240,300); // the size of the GUI window

            textField = new TextField();
            textField.setBounds(20,20,200,30); // the position in the GUI window and the width and height of the textField
            add(textField);

            textArea = new TextArea();
            textArea.setBounds(20,70,200,150);// the position in the GUI window and the width and height of the textArea
            add(textArea);

            button = new Button();
            button.setBounds(20,230,200,30);// the position in the GUI window and the width and height of the button
            button.setLabel("Append the text");
            button.addActionListener(new AddTextListener());
            add(button);
        }

        class AddTextListener implements ActionListener{ // the class responsible for adding the text to the text area

            @Override
            public void actionPerformed(ActionEvent e) {
                textArea.append(textField.getText()+" "); // an empty space added after each append for better visibility
            }
        }

    }

